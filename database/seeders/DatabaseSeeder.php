<?php

namespace Database\Seeders;
use App\Models\Publication;
use App\Models\User;
use App\Models\Comment;
// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        $testUser = User::factory()->create([
            'name' => 'Kakalatka Kozibrodka',
            'email' => 'mail_mail@mail.mail',
        ]);

        User::factory()->create([
            'name' => 'Jan Ratajczak',
            'email' => 'ranjatajczak@polmail.pl',
        ]);

        User::factory()->create([
            'name' => 'Iktor Wilczuk',
            'email' => 'hardstuckbronze@gmail.com',
        ]);

        User::factory()->create([
            'name' => 'Nigor Adkierniczny',
            'email' => 'lovecypher@gmail.com',
        ]);

        User::factory()->create([
            'name' => 'Al`kar Osama',
            'email' => 'gekkoislesbian@gmail.com',
        ]);

        User::factory()->create([
            'name' => 'Gerwazy Gerwazy',
            'email' => 'gerwazy@gerwazmail.gerwazy',
        ]);

        User::factory()->create([
            'name' => 'Daniel Brighton',
            'email' => 'hellothere@gmail.com',
        ]);

        User::factory()->create([
            'name' => 'Samy Oglozur',
            'email' => 'algebra@kebabmail.tr',
        ]);

        User::factory()->create([
            'name' => 'Victor Zeneki',
            'email' => 'archmage@gmail.com',
        ]);
        

        Publication::create([
            'title' => 'Iktor Wilczuk derankuje do irona?!',
            'content' => 'Po meczu z Al`karem Osamą który dźwigał całą drużynę kończąc z wynikiem 40/13/8 Iktor Wilczuk z wynikiem 0/25/1 zderankował do irona 3! Krąży także głos, że jego Mommy Sage go opuściła!',
            'author_id' => $testUser->id,
        ]);

        Publication::create([
            'title' => 'Piti nie może wytrzymać z jego teammatami!',
            'content' => 'Po instalockowaniu Yoru 12 gier pod rząd Piti jest zdenerwowany, że jego drużynia gra tylko duelistami. Oficjalnie stwierdził, że kończy karierę w Valorancie.',
            'author_id' => $testUser->id,
        ]);

        Publication::create([
            'title' => 'Recordzone wygrywa VCT!',
            'content' => 'Po ostrym starciu drużyny Recordzone, powiedzieli: jak przegramy jeden mecz, to Mierzyn przejmuje Szczecin. Tuż po tym, Al`kar, Nigor Adkierniczny, Piotr Chamber, Iktor Wilczuk i Karkol Lebanon wygrali 20 meczy pod rząd, i zdobyli pierwsze miejsce w VCT! ',
            'author_id' => $testUser->id,
        ]);

        Comment::create([
            'content' => 'Iktorowi na pewno należał się ten derank, po tym jak on ostatnio gra, każdemu chciało się płakać.',
            'author_id' => '2',
            'publication_id' => '1',
        ]);

        Comment::create([
            'content' => 'skill issue XDDDDD',
            'author_id' => '3',
            'publication_id' => '1',
        ]);

        Comment::create([
            'content' => 'I`m Polish man, Gorilla War',
            'author_id' => '4',
            'publication_id' => '1',
        ]);

        Comment::create([
            'content' => 'gg ez claps',
            'author_id' => '3',
            'publication_id' => '3',
        ]);

        Comment::create([
            'content' => 'recordzone gurom',
            'author_id' => '5',
            'publication_id' => '3',
        ]);

        Comment::create([
            'content' => 'Gerwazy',
            'author_id' => '6',
            'publication_id' => '3',
        ]);
        
    }
}
