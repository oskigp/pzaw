@extends('layouts.main')

@section('content')
    @foreach ($users as $u)
    <div class="mb-10">
        <p class="text-3xl">{{ $u->name }}</p><br>
        <p class="text-xl">{{ $u->email }}</p><br>
        <p>{{ $u->created_at }}</p><br>
        <p> Publikacje użytkownika: </p><br>
        @foreach($u->publications as $p)
        <p class="text-3xl m-5">{{$p->title}}</p>
        <a class="text-yellow-300 m-5" href="{{ route('show',['publication' => $p]) }}">Czytaj</a>
        @endforeach
    </div>
    @endforeach
@endsection