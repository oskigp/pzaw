@extends('layouts.main')

@section('content')

<form action="{{route('login')}}" method="POST">
    @csrf
    <div class="mt-3 ml-3 h-fit">
    <label class="text-xl @error('email') text-rose-800 @enderror">Nazwa</label> <br>
    <input class="bg-indigo-900 w-[50vh] rounded @error('email') border-solid border-2px border border-rose-800 @enderror" type="text" name="email" value="{{ old('email') }}">
    @error('email')
        <p class="text-rose-800 text-xs ml-3 mb-0 mt-0">{{ $message }}</p>
    @enderror
    
    </div>

    <div class="mt-3 ml-3 h-fit">
    <label class="text-xl @error('password') text-rose-800 @enderror">Hasło</label> <br>
    <input class="bg-indigo-900 w-[50vh] rounded @error('password') border-solid border-2px border border-rose-800 @enderror" type="password" name="password"><br>
    @error('password')
        <p class="text-rose-800 text-xs ml-3 mb-0 mt-0">{{ $message }}</p>
    @enderror

    </div>

    <div class="mt-3 ml-3 h-fit">
        <label class="text-xl">
        <input type="checkbox" name="remember">
        Zapamiętaj mnie</label>
    </div>

    <div class="mt-3 ml-3 h-fit">
    <button class="bg-indigo-900 mt-3 w-fit p-1 rounded" type="submit">Zaloguj</button>
    </div>
</form>

@endsection
