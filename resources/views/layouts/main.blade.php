<!DOCTYPE html>
<html lang="pl" class="dark">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <script src="https://cdn.tailwindcss.com"></script>
        <script src="https://unpkg.com/feather-icons"></script>
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <script>
            tailwind.config = {
                darkMode: 'class',
            }
        </script>
        <link href="{{ asset('/css/app.css') }}" rel="stylesheet">
        <style type="text/tailwindcss">
            @tailwind components;
            @layer components {
                .heading{
                    @apply bg-indigo-100 dark:bg-indigo-900 uppercase text-3xl font-bold text-slate-100 dark:text-slate-900 w-full p-2 mt-3 mb-3;
                }
            }
        </style>
        <title>{{config('app.name')}}</title>
    </head>
    <body class="dark bg-slate-100 dark:bg-slate-900 dark:text-white ">
        
        <nav class="flex dark bg-indigo-100 dark:bg-indigo-900 uppercase" >
            <img src="https://cdn.discordapp.com/attachments/900838594212659211/1161748304783220816/obraz_2023-10-11_213212874-modified_1.png?ex=65396d6c&is=6526f86c&hm=eb55ab38e0f35c8d8951200c95487ee49aa86d609f2e58e27b743bd156884ccc&" class="h-16 m-3 w-16"/>
            <a class="ml-8 mr-8 mt-3 mb-3 text-2xl flex align-bottom" href="{{route('home')}}" >Home<i class="w-7 h-7 ml-5" data-feather="home"></i> </a>
            <a class="ml-8 mr-8 mt-3 mb-3 text-2xl flex" href="{{route('about_us')}}">About us <i class="w-7 h-7 ml-5" data-feather="info"></i></a>
            <a class="ml-8 mr-8 mt-3 mb-3 text-2xl flex" href="{{route('quotecards')}}">Quotes<i class="w-7 h-7 ml-5" data-feather="book-open"></i></a>
            <a class="ml-8 mr-8 mt-3 mb-3 text-2xl flex" href="{{route('index')}}">Publications<i class="w-7 h-7 ml-5" data-feather="archive"></i></a>
            <a class="ml-8 mr-8 mt-3 mb-3 text-2xl flex" href="{{route('create')}}">Create<i class="w-7 h-7 ml-5" data-feather="edit-2"></i></a>
            <a class="ml-8 mr-8 mt-3 mb-3 text-2xl flex" href="{{route('users')}}">Users<i class="w-7 h-7 ml-5" data-feather="users"></i></a>
            @auth
                <a class="ml-auto mr-8 mt-3 mb-3 text-2xl flex" href="{{route('logout')}}">Logout<i class="w-7 h-7 ml-5" data-feather="log-out"></i></a>
            @else
            <a class="ml-auto mr-8 mt-3 mb-3 text-2xl flex" href="{{route('form')}}">Login<i class="w-7 h-7 ml-5" data-feather="log-in"></i></a>
            @endauth
            <a class="mr-8 mt-3 mb-3 text-2xl flex" href="{{route('create_account')}}">Register<i class="w-7 h-7 ml-5" data-feather="user-plus"></i></a>
        </nav>
        @include('alerts')
        @yield('content')
        <footer class="fixed bottom-0 w-full dark bg-indigo-100 dark:bg-indigo-900 h-auto text-xl text-center ">2023 Gerwazy™</footer>
    </body>
    <script defer>feather.replace();</script>
</html>