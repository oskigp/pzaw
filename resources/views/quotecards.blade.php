@extends('layouts.main')

@section('content')
<h1 class="dark bg-indigo-100 dark:bg-indigo-900 uppercase text-3xl font-bold text-slate-100 dark:text-slate-900 w-fit p-2 mt-3 mb-3" >volgorant</h1>
    <div class="flex justify-between w-full">
    @foreach($quotes as $q)
        <x-card hero="{{$q['hero']}}" quote="{{$q['quote']}}" role="{{$q['role']}}" image="{{$q['image']}}"/>
    @endforeach
    </div>
@endsection
