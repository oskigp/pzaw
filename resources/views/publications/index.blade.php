
@extends('layouts.main')

@section('content')  
    @foreach($publications as $p)
    <div class="mb-10 m-5">
        <p class="text-3xl">{{ $p->title }}</p><br>
        <p class="text-xl">{{$p->excerpt}}</p><br>
        <p class="text-xl">- {{ $p->author->name }}</p><br>
        <a class="text-yellow-300"href="{{ route('show',[$p]) }}">Czytaj więcej...</a>
    </div>
    @endforeach
@endsection
