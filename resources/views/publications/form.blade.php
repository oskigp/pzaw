@extends('layouts.main')

@php
    $title = old('title');
    $content = old('content');
    $author_id = old('author_id');
    $action = route('store');
    $page = "Tworzenie publikacji";

    if (! empty($publication)) {
        $action = route('update', [$publication]);
        $title = $publication->title;
        $content = $publication->content;
        $author_id = $publication->author_id;
        $page = "Edytowanie publikacji";

    }

@endphp


@section('content')
<form action="{{ $action }}" method="POST">
    @if (! empty($publication))
        @method('PUT')
    @endif
    <h3 class="text-5xl m-3"> {{$page}} </h3>
    @csrf
    <div class="mt-3 ml-3"><label class="text-xl @error('title') text-rose-800 @enderror">Tytuł</label><br><input value="{{ $title }}" type="text" name="title" class="bg-indigo-900 w-[50vh] rounded @error('title') border-solid border-2px border border-rose-800 @enderror"><br></div>

    @error('title')
	<p class="text-rose-800 text-xs ml-3">{{ $message }}</p>
    @enderror

    <div class="mt-3 ml-3"><label class="text-xl @error('content') text-rose-800 @enderror">Treść</label><br><textarea name="content" class="bg-indigo-900 w-[50vh] h-[30vh] rounded @error('content') border-solid border-2px border border-rose-800 @enderror">{{ $content }}</textarea><br></div>

    @error('content')
	<p class="text-rose-800 text-xs ml-3">{{ $message }}</p>
    @enderror

    <div class="mt-3 ml-3">
        <label class="text-xl @error('author_id') text-rose-800 @enderror">Autor</label><br>
        <select name="author_id" class="bg-indigo-900 w-[50vh] rounded @error('author_id') border-solid border-2px border border-rose-800 @enderror">
            <option value="">~~Wybierz autora~~</option>
            @foreach($users as $u)
                <option value="{{ $u->id }}" @selected($author_id == $u->id)>{{ $u->name }}</option>

            @endforeach
        </select>
    </div>
    

    @error('author_id')
	<p class="text-rose-800 text-xs ml-3">{{ $message }}</p>
    @enderror

    <div class="bg-indigo-900 m-3 w-fit p-1 rounded"><input class="text-xl" type="submit" value="Zapisz"></div>
</form>
@endsection