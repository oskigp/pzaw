@extends('layouts.main')

@section('content')
    <p class="text-5xl m-5">{{$publication->title}}</p>
    <p class="text-3xl m-5">{{$publication->content}}</p>
    <p class="text-3xl m-5">- {{ $publication->author->name }}</p>
    <p class="text-xl m-5">  {{$date}}</p>
    <div class="flex">
        <a href="{{ route('edit', [$publication]) }}"><p class="text-yellow-300 border border-yellow-300 ml-5 w-[8vh] rounded text-center text-xl">Edytuj</p></a>
        <form action="{{ route('destroy', [$publication]) }}" method="POST">
            @csrf
            @method('DELETE')
            <button class="text-rose-800 border border-rose-800 rounded w-[8vh] ml-3 text-center text-xl" type="submit">Usuń</button>
        </form>
    </div>
    @foreach($comments as $c)
    <p class="text-xl m-2"> {{$c->author->name}} </p>
    <p class="mt-2 mb-5 ml-4"> {{$c->content}} </p>
    @endforeach
@endsection
