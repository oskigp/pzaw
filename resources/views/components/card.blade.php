@props(['hero', 'quote', 'role', 'image'])

  <div class="dark max-w-sm bg-indigo-100 dark:bg-indigo-900 border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 flex flex-col m-5">
    <img class="w-50 h-50 mb-3 object-center" src="{{$image}}" alt="" />
    <div class="p-5">
        <h5 class="mb-2 text-5xl font-bold tracking-tight text-black dark:text-white text-center">{{$hero}}</h5>
        <h5 class="mb-2 text-3xl font-bold tracking-tight text-black dark:text-white text-center">{{$role}}</h5>
        <p class="mb-3 text-2xl font-normal text-gray-700 dark:text-gray-400">{{$quote}}</p>
    </div>
</div>

