@extends('layouts.main')

@section('content')
    <h1 class="heading"><p class="m-auto w-fit">Gerwazy</p></h1>
    <p>Dzisiaj jest {{ $date }}</p>
    <div class="m-5">
        <p class="text-3xl">{{ $publication[0]->title }}</p><br>
        <p class="text-xl">{{$publication[0]->excerpt}}</p><br>
        <p class="text-xl">- {{ $publication[0]->author->name }}</p><br>
        <a class="text-yellow-300 text-2xl"href="{{ route('show',[$publication[0]]) }}">Czytaj więcej...</a>
    </div>
@endsection

