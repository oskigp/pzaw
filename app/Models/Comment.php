<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int $id
 * @property string $content
 * @property int $author_id
 * @property int $publication_id
 * 
 * @property-read User $author
 */

class Comment extends Model
{
    protected $fillable = [
        'content',
        'author_id',
        'publication_id',
    ];

    public function author()
    {
	    return $this->belongsTo(User::class, 'author_id');
    }
    
}
