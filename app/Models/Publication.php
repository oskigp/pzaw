<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Casts\Attribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations;


/**
 * @property int $id
 * @property string $content
 * @property string $title
 * @property int $author_id
 * 
 * @property-read User $author
 * @property-read Comment $comments
 */



class Publication extends Model
{
    protected $fillable = [
        'title',
        'content',
        'author_id'
    ];

    protected function excerpt(): Attribute {
        return Attribute::make(
            get: fn () => strlen($this->content) > 60 ? substr($this->content, 0, 60) . '...' : $this->content,
        );
    }

    public function author()
    {
	    return $this->belongsTo(User::class, 'author_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }


}
