<?php

namespace App\Http\Controllers;
use App\Models\Publication;
use Illuminate\Http\Request;
use App\Models\User;

class SiteController extends Controller
{
    public function home()
    {
        $newestPublication = (Publication::orderByDesc('created_at')
            ->limit(1))->get();

        return view('home', [
            'date' => now(),
            'publication' => $newestPublication,
        ]);
    }

    public function quotecards()
     {
        $quotes = [
            1 => [
                'quote' => 'Buy stuff, kaching, lil` skkkrrrr, then we`re done, yeah?',
                'hero' => 'Phoenix',
                'role' => 'Duelist',
                'image' => 'https://cdn.discordapp.com/attachments/900838594212659211/1161348467797536991/latest.png?ex=6537f90b&is=6525840b&hm=9d586b25c25ab02a912856c54954bed597fab5852094ab3ed0556688664032ab&',
            ],
            2 => [
                'quote' => 'One of my cameras is broken!... Oh, wait... Okay. It`s fine.',
                'hero' => 'Cypher',
                'role' => 'Sentinel',
                'image' => 'https://cdn.discordapp.com/attachments/900838594212659211/1161348523237855363/latest.png?ex=6537f918&is=65258418&hm=8336cb076ae6e1108b4d09e2a4a8d3a020e6e5ef6295917e807d15a700e85fa4&',
            ],
            3 => [
                'quote' => 'Oh, my back hurts! Everyone`s so heavy!',
                'hero' => 'Jett',
                'role' => 'Duelist',
                'image' => 'https://media.discordapp.net/attachments/900838594212659211/1161348673284870274/latest.png?ex=6537f93c&is=6525843c&hm=97fa8e8d065c9f21078b48c53b40c9e33513bddf325f7747886ec64629b2debb&=',
            ],
            4 => [
                'quote' => 'Don`t stress if I die. It was great knowing you all. Also, delete my hard drive.',
                'hero' => 'Killjoy',
                'role' => 'Sentinel',
                'image' => 'https://media.discordapp.net/attachments/900838594212659211/1161348723226443938/latest.png?ex=6537f948&is=65258448&hm=252580c02f05063c5c02f0cdd42f632385c7be28a16719ce385800579bdb872d&=',
            ],
            5 => [
                'quote' => 'Do not worry my radiant friends. I will unplug their talking toaster.',
                'hero' => 'Chamber',
                'role' => 'Chamber.',
                'image' => 'https://cdn.discordapp.com/attachments/900838594212659211/1161348625977315369/latest.png?ex=6537f931&is=65258431&hm=799557450f3061aa2bfcc16fc32e97c063c583fb3b2eccb49772514467bab7d9&',
            ],
        ];
        return view('quotecards', [
            'quotes' => $quotes, 
        ]);
    }

    public function about_us()
    {
        return view('about_us', []);
    }

    public function users() {
        return view('users/users', [
            'users' => User::all(),
        ]);
    }
}
