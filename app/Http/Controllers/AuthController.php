<?php

namespace App\Http\Controllers;
use App\Http\Requests\LoginRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function form(){
        return view('users/login', [
            
            
        ]);
    }

    public function login(LoginRequest $request){
        $credentials = [
            'email' => $request->validated('login'),
            'password' => $request->validated('password'),
        ];
        
        if (Auth::attempt($credentials, $request->boolean('remember_me'))) {
            $request->session()->regenerate();

            return redirect()->intended('/');
        }

        else {
            return back()->withErrors([
                'login' => 'Login lub hasło nie są poprawne.'
            ]);
        }
        
    }

    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/')->with('success', 'Pomyślnie wylogowano');
    }
}
