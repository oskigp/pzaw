<?php

namespace App\Http\Controllers;
use App\Models\Publication;
use App\Models\User;
use App\Models\Comment;
use App\Http\Requests\StorePublicationRequest;
use App\Http\Requests\UpdatePublicationRequest;
use Carbon\Carbon;

class PublicationController extends Controller
{
    public function index()
    {
        $publications = Publication::all();
        
        return view('publications/index', [
            'publications' => $publications
        ]);

        
    }

    public function show(Publication $publication)
    {
        Carbon::setLocale(config('app.locale'));
        $time_ago = Carbon::create($publication->created_at)->diffForHumans(Carbon::now());

        if (empty($publication)) {
            abort(404);
        }
    
        return view('publications/show', [
            'publication' => $publication, 
            'comments' => $publication->comments,
            'date' => $time_ago,
        ]);
    }

    public function create()
    {
        $users = User::all();
        return view('publications/form', [
            'users' => $users,
        ]);
    }

    public function store(StorePublicationRequest $request)
    {
        $data = $request->validated();
        $newPublication = new Publication($data);
        $newPublication->save();
        return redirect()->route('show', [$newPublication])->with('success', 'Publikacja została dodana');
    }

    public function edit(Publication $publication)
    {
        $users = User::all();
        return view('publications.form', [
            'publication' => $publication,
            'users' => $users,
        ]);
    
    }

    public function update(UpdatePublicationRequest $request, Publication $publication)
    {   
        $data = $request->validated();
        $publication->fill($data);
        $publication->save();
        return redirect()->route('show', [$publication])->with('success', 'Publikacja została zaktualizowana');
    }

    public function destroy(Publication $publication)
    {
        foreach($publication->comments as $c){
            $c->delete();
        }

        $publication->delete();

        return redirect()->route('index')->with('success', 'Publikacja została usunięta');

    }
}
