<?php

namespace App\Http\Controllers;
use App\Http\Requests\RegisterRequest;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function create_account(){
        return view('users/create_account', [

        ]);
    }

    public function register(RegisterRequest $request){
        $data = $request->validated();
        $newUser = new User($data);
        $newUser->save();
        return redirect()->route('users')->with('success', 'Zarejestrowano');
    }

    public function login(){
        return view('users/login', [

        ]);
    }
}
