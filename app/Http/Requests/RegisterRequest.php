<?php

namespace App\Http\Requests;
use Illuminate\Validation\Rules\Password;
use Illuminate\Foundation\Http\FormRequest;

class RegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'name' => ['required', 'string', 'min:3', 'max:20', 'unique:users,name'],
            'email' => ['required', 'string', 'unique:users,email'],
            'password' => ['confirmed', 'required', 'string', 'min:3', 'max:20', Password::min(8)->mixedCase(), Password::min(8)->numbers(), Password::min(8)->symbols() ],
        ];
    }

    public function messages(): array
    {
        return [
            '*.required' => 'Pole musi być wypełnione.',
            'name.min' => 'Nazwa musi mieć co najmniej 3 znaki.',
            'name.max' => 'Nazwa nie może mieć więcej niż 20 znaków.',
            'name.unique' => 'Ta nazwa jest już zajęta.',
            'email.unique' => 'Ten adres email jest już użyty.',
            'password.min' => 'Hasło musi mieć co najmniej 3 znaki.',
            'password.max' => 'Hasło nie może mieć więcej niż 10 znaków.',
            'password.confirmed' => 'Hasła się różnią.',
            'password.mixedCase' => 'Hasło musi zawierać wielkie i małe litery.',
            'password.numbers' => 'Hasło musi zawierać liczbę.',
            'password.symbols' => 'Hasło musi zawierać symbol.',
        ];
    }
}
