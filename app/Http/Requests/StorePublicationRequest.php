<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePublicationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'title' => ['required', 'string', 'min:3', 'max:50'],
            'content' => ['required', 'string', 'min:10', 'max:500'],
            'author_id' => ['required', 'numeric','exists:App\Models\User,id'],
        ];
    }

    public function messages(): array
    {
        return [
            '*.required' => 'Pole musi być wypełnione.',
            'title.min' => 'Tytuł musi mieć co najmniej 3 znaki.',
            'title.max' => 'Tytuł nie może mieć więcej niż 50 znaków.',
            'content.min' => 'Treść musi mieć co najmniej 10 znaków.',
            'content.max' => 'Treść nie może mieć więcej niż 500 znaków.',
            'author_id.required' => 'Nie wybrano autora.',
            'author_id.exists' => 'Autor nie istnieje.',
        ];
    }
}
