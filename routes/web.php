<?php

use Illuminate\Support\Facades\Route;

Route::get('greetings/{name?}', function ($name = null) {
    if (empty($name)) {
        echo "Witaj! Jak masz na imię?";
    } else {
        echo "Cześć $name! Miło mi Ciebie poznać :)";
    }
})->whereAlpha('name');

Route::get('admin-panel/{code}', function ($code) {
    abort_if($code != 'test123', 401, 'Podano nieprawidłowy kod dostępu.');
   
    echo "Dostęp przyznany";
})->whereAlphaNumeric('code');

Route::prefix('calculator')->group(function () {

    Route::get('/add/{num1}/{num2}', function ($num1, $num2) {
        echo $num1 + $num2;
    })->WhereNumber('num1', 'num2');
    
    Route::get('/substract/{num1}/{num2}', function ($num1, $num2) {
        echo $num1 - $num2;
    })->WhereNumber('num1', 'num2');
    
    Route::get('/multiply/{num1}/{num2}', function ($num1, $num2) {
        echo $num1 * $num2;
    })->WhereNumber('num1', 'num2');
    
    Route::get('/divide/{num1}/{num2}', function ($num1, $num2) {
        echo $num1 / $num2;
    })->WhereNumber('num1', 'num2');
    
    Route::get('/power/{num1}/{num2}', function ($num1, $num2) {
        echo $num1 ** $num2;
    })->WhereNumber('num1', 'num2');
    
    Route::get('/max/{num1}/{num2}', function ($num1, $num2) {
        echo max(array($num1, $num2));
    })->WhereNumber('num1', 'num2');

    Route::get('/min/{num1}/{num2}', function ($num1, $num2) {
        echo min(array($num1, $num2));
    })->WhereNumber('num1', 'num2');

});

Route::get('reverse/{str}', function ($str) {
    $str = str_replace("_", " ", $str);
    echo strrev($str);
});

$quotes = [
    1 => [
        'quote' => 'Buy stuff, kaching, lil` skkkrrrr, then we`re done, yeah?',
        'hero' => 'Phoenix',
        'role' => 'Duelist',
        'image' => 'https://cdn.discordapp.com/attachments/900838594212659211/1161348467797536991/latest.png?ex=6537f90b&is=6525840b&hm=9d586b25c25ab02a912856c54954bed597fab5852094ab3ed0556688664032ab&',
    ],
    2 => [
        'quote' => 'One of my cameras is broken!... Oh, wait... Okay. It`s fine.',
        'hero' => 'Cypher',
        'role' => 'Sentinel',
        'image' => 'https://cdn.discordapp.com/attachments/900838594212659211/1161348523237855363/latest.png?ex=6537f918&is=65258418&hm=8336cb076ae6e1108b4d09e2a4a8d3a020e6e5ef6295917e807d15a700e85fa4&',
    ],
    3 => [
        'quote' => 'Oh, my back hurts! Everyone`s so heavy!',
        'hero' => 'Jett',
        'role' => 'Duelist',
        'image' => 'https://media.discordapp.net/attachments/900838594212659211/1161348673284870274/latest.png?ex=6537f93c&is=6525843c&hm=97fa8e8d065c9f21078b48c53b40c9e33513bddf325f7747886ec64629b2debb&=',
    ],
    4 => [
        'quote' => 'Don`t stress if I die. It was great knowing you all. Also, delete my hard drive.',
        'hero' => 'Killjoy',
        'role' => 'Sentinel',
        'image' => 'https://media.discordapp.net/attachments/900838594212659211/1161348723226443938/latest.png?ex=6537f948&is=65258448&hm=252580c02f05063c5c02f0cdd42f632385c7be28a16719ce385800579bdb872d&=',
    ],
    5 => [
        'quote' => 'Do not worry my radiant friends. I will unplug their talking toaster.',
        'hero' => 'Chamber',
        'role' => 'Chamber.',
        'image' => 'https://cdn.discordapp.com/attachments/900838594212659211/1161348625977315369/latest.png?ex=6537f931&is=65258431&hm=799557450f3061aa2bfcc16fc32e97c063c583fb3b2eccb49772514467bab7d9&',
    ],
];


Route::get('quotes/{id?}', function ($id = null) use($quotes){

    
    if ($id == null){
        $id = rand(1, count($quotes));
    }

    if (array_key_exists($id, $quotes)) {
        echo $quotes[$id]['quote'];
    }
    else {
        abort(404);
    }

        
})->WhereNumber('id');

Route::get('quotes/{id}/hero', function($id) use($quotes){

    echo $quotes[$id]['quote'] . " - " . $quotes[$id]['hero'];

})->WhereNumber('id');

Route::get('quotes/{id}/guess/{hero}', function($id, $hero) use($quotes){
    
    if(strcasecmp($hero, $quotes[$id]['hero']) == 0) {
        echo "Poprawna odpowiedź!";
    }
    else {
        echo "Zła odpowiedź.";
    }

})->WhereNumber('id');

/*$newestPublication = Publication::all()
    ->sortBy('created_at')
    ->take(1);
*/



use App\Models\Publication;
use App\Http\Controllers\PublicationController;
use App\Http\Controllers\SiteController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AuthController;

Route::get('/about-us', [SiteController::class, 'about_us'])->name('about_us');

Route::get('/publications', [PublicationController::class, 'index'])->name('index');

Route::get('/quotecards', [SiteController::class, 'quotecards'])->name('quotecards');

Route::get('/', [SiteController::class, 'home'])->name('home');
        
Route::get('/publications/{publication}', [PublicationController::class, 'show'])->WhereNumber('publication')->name('show');

Route::get('/users', [SiteController::class, 'users'])->name('users');

Route::get('/publications/create', [PublicationController::class, 'create'])->name('create')->middleware('auth');

Route::post('/store', [PublicationController::class, 'store'])->name('store')->middleware('auth');

Route::get('post/{publication}/edit', [PublicationController::class, 'edit'])->name('edit')->middleware('auth');

Route::put('post/{publication}', [PublicationController::class, 'update'])->name('update')->middleware('auth');

Route::delete('publications/{publication}', [PublicationController::class, 'destroy'])->name('destroy')->middleware('auth');

Route::get('/create_account', [UserController::class, 'create_account'])->name('create_account');

Route::post('/register', [UserController::class, 'register'])->name('register');

Route::get('/form', [AuthController::class, 'form'])->name('form');

Route::post('/login', [AuthController::class, 'login'])->name('login');








?>